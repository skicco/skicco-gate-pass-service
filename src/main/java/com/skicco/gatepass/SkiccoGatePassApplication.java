package com.skicco.gatepass;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EnableAutoConfiguration
@EntityScan(basePackages="com.skicco.commonentity")
@ComponentScan(basePackages="com.skicco")
@EnableJpaRepositories(basePackages="com.skicco.dao")
@SpringBootApplication
public class SkiccoGatePassApplication {

	public static void main(String[] args) {
		SpringApplication.run(SkiccoGatePassApplication.class, args);
	}

}
