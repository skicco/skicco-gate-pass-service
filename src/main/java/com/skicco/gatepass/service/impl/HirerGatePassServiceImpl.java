package com.skicco.gatepass.service.impl;

import com.skicco.commonentity.Hirer;
import com.skicco.constants.SkiccoConstants;
import com.skicco.dao.HirerDao;
import com.skicco.gatepass.service.HirerGatePassService;
import com.skicco.util.ResponseEnum;
import com.skicco.util.Utility;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Service
@Slf4j
public class HirerGatePassServiceImpl implements HirerGatePassService {

    @Autowired
    HirerDao hirerDao;

    @Override
    public Map<String, Object> signUp(Hirer hirer) {

        Map<String, Object> map = new HashMap<>();
        log.info("SignUp Service...!!!");
        String emailId = hirer.getEmail();
        /*
         * Check whether the emailId already exists in database. If exists,
         * then send a message, saying "emailId already exists"
         */
        if (StringUtils.isNotEmpty(emailId == null ? emailId : emailId.trim())) {
            Hirer userObj = hirerDao.findByEmail(emailId);
            if (null == userObj) {
                log.info("EmailId doesnt exists...!!!");
                hirerDao.save(prepareNewHirerPersistObject(hirer));
                map.put("responseCode", ResponseEnum.EMAIL_SENT.value());
                map.put("huid", hirer.getHuid());
            } else {
                String status = userObj.getStatus();
                if (status.equalsIgnoreCase("Not Authorized")) {
                    map.put("responseCode", ResponseEnum.NOT_AUTHORIZED.value());
                } else if (status.equals("Authorized")) {
                    map.put("responseCode", ResponseEnum.AUTHORIZED.value());
                } else if (status.equalsIgnoreCase("Hirer")) {
                    map.put("responseCode", ResponseEnum.SKICON.value());
                }

            }
        }
        return map;
    }

    private Hirer prepareNewHirerPersistObject(Hirer hirer) {
        hirer.setHuid(UUID.randomUUID().toString() + "-" + StringUtils.lowerCase(Utility.base64Encode(hirer.getEmail()))
                .replaceAll("=", ""));
        hirer.setStatus(SkiccoConstants.NOT_AUTHORIZED);
        return hirer;
    }

}

