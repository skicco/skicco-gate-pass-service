package com.skicco.gatepass.service;

import com.skicco.commonentity.Hirer;

import java.util.Map;

public interface HirerGatePassService {

    Map<String, Object> signUp(Hirer hirer);

}
