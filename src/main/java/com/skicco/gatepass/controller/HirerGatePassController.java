package com.skicco.gatepass.controller;


import com.skicco.commonentity.Hirer;
import com.skicco.commonentity.Response;
import com.skicco.gatepass.mapper.HirerMapper;
import com.skicco.gatepass.service.HirerGatePassService;
import com.skicco.skiccoshareddto.HirerDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/gate-pass")
@CrossOrigin
@Slf4j
public class HirerGatePassController {

    @Autowired
    HirerGatePassService hirerGatePassService;

    @PostMapping(value = "/hirerSignup")
    public @ResponseBody
    ResponseEntity<Response> signUp(@RequestBody HirerDTO hirerDTO) {
        Hirer hirer  = HirerMapper.hirerDtoToEntityMapper(hirerDTO);
        Map<String, Object> response = hirerGatePassService.signUp(hirer);
        Response responseObj = new Response();
        log.info("Hirer Signup....Hirer : " + hirer.getName() + " !!!");
        Integer responseCode = (Integer)response.get("responseCode");

        if (responseCode != null) {
            responseObj.setResponseCode(responseCode);
            return new ResponseEntity<Response>(responseObj, HttpStatus.OK);
        }
        return null;
    }

}
