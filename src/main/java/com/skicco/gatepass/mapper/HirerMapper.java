package com.skicco.gatepass.mapper;


import com.skicco.commonentity.Hirer;
import com.skicco.constants.SkiccoConstants;
import com.skicco.skiccoshareddto.HirerDTO;
import com.skicco.util.Utility;
import org.apache.commons.lang3.StringUtils;

import java.util.UUID;

public class HirerMapper {

    public static Hirer hirerDtoToEntityMapper(HirerDTO hirerDTO) {
        return Hirer.builder()
                .email(hirerDTO.getEmailId())
                .password(hirerDTO.getPassword())
                .status(hirerDTO.getStatus())
                .huid(hirerDTO.getHuid())
                .build();
    }
}
